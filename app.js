const bankBalanceElement = document.getElementById("bankBalance");
const getLoanButtonElement = document.getElementById("getLoanButton");
const payAmountElement = document.getElementById("payAmount");
const addToBankButtonElement = document.getElementById("addToBankButton");
const workButtonElement = document.getElementById("workButton");
const selectLaptopElement = document.getElementById("selectLaptop");
const laptopFeaturesElement = document.getElementById("laptopFeatures");
const laptopImgElement = document.getElementById("laptopImg");
const laptopNameElement = document.getElementById("laptopName");
const laptopDescriptionElement = document.getElementById("laptopDescription");
const laptopPriceElement = document.getElementById("laptopPrice");
const priceCurrencyTextElement = document.getElementById("priceCurrencyText")
const buyLaptopButtonElement = document.getElementById("buyLaptopButton");
const loanBalanceElement = document.getElementById("loanBalance")
const loanBalanceLabelElement = document.getElementById("loanBalanceLabel")
const loanMinusTextElement = document.getElementById("loanMinusText");
const loanBalanceCurrencyLabelElement = document.getElementById("loanBalanceCurrencyLabel");
const repayLoanButtonElement = document.getElementById("repayLoanButton");

const imagesApiUrl = 'https://noroff-komputer-store-api.herokuapp.com/';

//Handle laptop selection
let laptops = [];

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

const addLaptopsToMenu = laptops => {
    laptops.forEach(x => addLaptopToMenu(x));
}

const addLaptopToMenu = laptop => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    selectLaptopElement.appendChild(laptopElement);
}

const handleLaptopMenuChange = e => {
    laptopFeaturesElement.innerText = "";
    const selectedLaptop = laptops[e.target.selectedIndex-1];
    
    changeLaptopCardElements(selectedLaptop);
    changeJumbotronElements(selectedLaptop);
}

const changeLaptopCardElements = selectedLaptop => {
    selectedLaptop.specs.forEach(x => fillLaptopDescription(x));
}

const changeJumbotronElements = selectedLaptop => {
    laptopNameElement.innerText = selectedLaptop.title;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    priceCurrencyTextElement.removeAttribute("hidden");
    laptopPriceElement.innerText = selectedLaptop.price;
    buyLaptopButtonElement.removeAttribute("hidden");
    getImage(selectedLaptop);
}

const fillLaptopDescription = laptopDesc => {    
    const laptopDescItem = document.createElement("li");
    laptopDescItem.innerText = laptopDesc;
    laptopFeaturesElement.appendChild(laptopDescItem);
}

const getImage = selectedLaptop => {
    let img = new Image();

    img.src = imagesApiUrl + selectedLaptop.image;

    img.onload = function()  {
        laptopImgElement.src = img.src
    }

    img.onerror = function() {       

        laptopImgElement.src = './img/404notFound.png';
    }
}

selectLaptopElement.addEventListener("change", handleLaptopMenuChange);
//END Handle laptop selection

//Work and Bank
let outstandingLoan = 0.0;
let hasLoan = false;

let payBalance = 0.0;
let bankBalance = 0.0;
const payIncrement = 100;

const updatePayAmount = () => {
    payBalance += payIncrement;
    setTextElements();
}

const transferToBank = () => {    

    if(outstandingLoan > 0){
        let payToLoanValue = payBalance * 0.1;        
        let transferToBankValue = payBalance * 0.9;
        if(outstandingLoan - payToLoanValue < 0){
            let excessAmount = (outstandingLoan - payToLoanValue) * -1;
            payToLoanValue -= excessAmount;
            transferToBankValue += excessAmount;
        }
                
        outstandingLoan -= payToLoanValue;          
        bankBalance += transferToBankValue;
        payBalance = 0;

        checkLoanStatus();
        setTextElements();
        checkLoanElements();
    } else {
        bankBalance += payBalance;
        payBalance = 0;
        checkLoanElements();
        setTextElements();


        
    }
}


const getLoanValue = () => {
    let loanValue = parseFloat(prompt("Please enter desired loan amount:", ""));
    if(Number.isNaN(loanValue)) {
        alert("Invalid input. Please input a number");
    } else {
        validateLoanValue(loanValue);
    }

}

const validateLoanValue = loanValue => {
    if((loanValue) > (bankBalance * 2)) {
        alert("Desired loan will put you in too much debt. \nLoan denied.");
    } else if (hasLoan === true) {
        alert("You must pay off your current loan before you can take another");
    }
     else {
        updateLoanValues(loanValue);
    }
}

const updateLoanValues = loanValue => {
    outstandingLoan = loanValue;
    bankBalance += loanValue;
    
    checkLoanStatus();
    checkLoanElements();
    setTextElements();

}

const checkLoanStatus = () => {
    if(outstandingLoan === 0){
        hasLoan = false;
    } else {
        hasLoan = true;
    }
}

const payOffLoan = () => {
    let payToLoanValue = payBalance;
    if(outstandingLoan - payToLoanValue < 0){
        let excessAmount = (outstandingLoan - payToLoanValue) * -1;
        payToLoanValue -= excessAmount;
        outstandingLoan -= payToLoanValue;
        payBalance = excessAmount;
    } else {
         outstandingLoan -= payBalance;
         payBalance = 0;
    }

    checkLoanStatus();
    checkLoanElements();
    setTextElements();
        
}


const checkLoanElements = () => {
    if(outstandingLoan === 0) {
        loanBalanceElement.setAttribute("hidden", "");
        loanBalanceLabelElement.setAttribute("hidden", "");
        loanBalanceCurrencyLabelElement.setAttribute("hidden", "");
        loanMinusTextElement.setAttribute("hidden", "");
        repayLoanButtonElement.setAttribute("hidden", "");
        
    } else {
        loanBalanceElement.removeAttribute("hidden");
        loanBalanceLabelElement.removeAttribute("hidden");
        loanBalanceCurrencyLabelElement.removeAttribute("hidden");
        loanMinusTextElement.removeAttribute("hidden");
        repayLoanButtonElement.removeAttribute("hidden");        
    }

}
const setTextElements = () => {
    loanBalanceElement.innerText = outstandingLoan; 
    bankBalanceElement.innerText = bankBalance;
    payAmountElement.innerText = payBalance;
}

repayLoanButtonElement.addEventListener("click", payOffLoan);
getLoanButtonElement.addEventListener("click", getLoanValue);
addToBankButtonElement.addEventListener("click", transferToBank);
workButtonElement.addEventListener("click", updatePayAmount);
//END Work and Bank

//Buy laptop
const buyLaptop = () => {
    const price = parseFloat(laptopPriceElement.textContent);
    const name = laptopNameElement.textContent;
    if(price > bankBalance) {
        alert("You don't have enough money in the bank to buy this laptop!");
    } else {
        alert(`Transcation complete! Congrats on your new ${name}.`);
        bankBalance -= price;
        setTextElements();
    }
}

buyLaptopButtonElement.addEventListener("click", buyLaptop);
//END Buy laptop
